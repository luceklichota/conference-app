import {
    aws_lambda as lambda,
    aws_cognito as cognito, Duration, CfnOutput,
} from 'aws-cdk-lib';

import * as apigw from '@aws-cdk/aws-apigatewayv2-alpha'
import {HttpLambdaIntegration} from '@aws-cdk/aws-apigatewayv2-integrations-alpha'

import {Construct} from "constructs";
import {CorsHttpMethod, HttpMethod} from "@aws-cdk/aws-apigatewayv2-alpha";
import {HttpUserPoolAuthorizer} from "@aws-cdk/aws-apigatewayv2-authorizers-alpha";

interface ApplicationAPIProps {
    cityService: lambda.IFunction;
    venuesService: lambda.IFunction;
    eventsService: lambda.IFunction;
    userService: lambda.IFunction;
    userPool: cognito.IUserPool;
    userPoolClient: cognito.IUserPoolClient;
}

export class ApplicationAPI extends Construct {
    public readonly httpApi: apigw.HttpApi;

    constructor(scope: Construct, id: string, props: ApplicationAPIProps) {
        super(scope, id);

        const serviceMethods = [
            HttpMethod.GET,
            HttpMethod.POST,
            HttpMethod.DELETE,
            HttpMethod.PUT,
            HttpMethod.PATCH,
        ];

        // API Gateway ------------------------------------------------------

        this.httpApi = new apigw.HttpApi(this, 'HttpProxyApi', {
            apiName: 'serverless-api',
            createDefaultStage: true,
            corsPreflight: {
                allowHeaders: ['Authorization', 'Content-Type', '*'],
                allowMethods: [
                    CorsHttpMethod.GET,
                    CorsHttpMethod.POST,
                    CorsHttpMethod.DELETE,
                    CorsHttpMethod.PUT,
                    CorsHttpMethod.PATCH,
                ],
                allowOrigins: ['http://localhost:4200', 'http://*'],
                allowCredentials: true,
                maxAge: Duration.days(10),
            },
        });

        // Authorizer -------------------------------------------------------

        const authorizer = new HttpUserPoolAuthorizer('ApiAuthorizer', props.userPool, {
            userPoolClients: [props.userPoolClient]
        });

        const cityServiceIntegration = new HttpLambdaIntegration('EventIntegration', props.cityService);

        this.httpApi.addRoutes({
            path: `/user-city/{proxy+}`,
            methods: serviceMethods,
            integration: cityServiceIntegration,
            authorizer,
        });

        this.httpApi.addRoutes({
            path: `/cities/{proxy+}`,
            methods: serviceMethods,
            integration: cityServiceIntegration,
            authorizer,
        });

        const venuesServiceIntegration = new HttpLambdaIntegration('VenuesIntegration', props.venuesService);

        this.httpApi.addRoutes({
            path: `/venues/{proxy+}`,
            methods: serviceMethods,
            integration: venuesServiceIntegration,
            authorizer,
        });

        const eventsServiceIntegration = new HttpLambdaIntegration('VenuesIntegration', props.eventsService);

        this.httpApi.addRoutes({
            path: `/events/{proxy+}`,
            methods: serviceMethods,
            integration: eventsServiceIntegration,
            authorizer,
        });

        const userServiceIntegration = new HttpLambdaIntegration('UserIntegration', props.userService);

        this.httpApi.addRoutes({
            path: `/user/{proxy+}`,
            methods: serviceMethods,
            integration: userServiceIntegration,
            authorizer,
        });

        // Outputs -----------------------------------------------------------

        new CfnOutput(this, 'URL', {
            value: this.httpApi.apiEndpoint
        });
    }
}
