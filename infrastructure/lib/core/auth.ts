import {
    aws_lambda as lambda,
    aws_cognito as cognito,
    aws_cloudfront as cloudfront
} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {
    ProviderAttribute,
    UserPool,
    UserPoolClient,
    UserPoolClientIdentityProvider, UserPoolDomain,
    UserPoolIdentityProviderGoogle
} from "aws-cdk-lib/aws-cognito";
import * as process from "process";
import * as path from "path";


interface AuthProps {
    googleId: string
    googleSecret: string
    webDistribution: cloudfront.CloudFrontWebDistribution
}

export class Auth extends Construct {
    public readonly userPool: UserPool
    public readonly client: UserPoolClient
    public readonly domain: UserPoolDomain

    constructor(scope: Construct, id: string, authProps: AuthProps) {
        super(scope, id);

        const prefix = `conference-${process.env.ENV}`;

        const preSignup = new lambda.Function(this, 'preSignup', {
            runtime: lambda.Runtime.NODEJS_14_X,
            handler: 'index.handler',
            code: lambda.Code.fromAsset(path.join(__dirname, '../../../services/signup-restrictions')),
        });

        this.userPool = new UserPool(this, "Conference", {
            lambdaTriggers: {
                preSignUp: preSignup
            },
            customAttributes: {
                'city': new cognito.StringAttribute({ minLen: 3, maxLen: 15, mutable: true }),
            }
        });

        new UserPoolIdentityProviderGoogle(this, "Google", {
            userPool: this.userPool,
            clientId: authProps.googleId,
            clientSecret: authProps.googleSecret,
            scopes: ["email", "profile"],
            attributeMapping: {
                email: ProviderAttribute.GOOGLE_EMAIL,
                givenName: ProviderAttribute.GOOGLE_GIVEN_NAME,
                familyName: ProviderAttribute.GOOGLE_FAMILY_NAME,
            },
        });

        const callbackUrls = [`https://${authProps.webDistribution.distributionDomainName}/token`];
        if (process.env.ENV === 'staging') {
            callbackUrls.push('http://localhost:4200/token');
        }

        this.client = new UserPoolClient(this, "UserPoolClient", {
            userPool: this.userPool,
            generateSecret: false,
            supportedIdentityProviders: [UserPoolClientIdentityProvider.GOOGLE],
            oAuth: {
                callbackUrls: callbackUrls,
                logoutUrls: callbackUrls,
            },
        });

        this.domain = this.userPool.addDomain("default", {
            cognitoDomain: {
                domainPrefix: prefix,
            },
        });

        new cognito.CfnUserPoolGroup(this, 'AdminGroup', {
            userPoolId: this.userPool.userPoolId,
            groupName: 'admin',
            precedence: 1,
            description: 'Admin users',
        });

        new cognito.CfnUserPoolGroup(this, 'PresenterGroup', {
            userPoolId: this.userPool.userPoolId,
            groupName: 'presenter',
            precedence: 5,
            description: 'Presenters',
        });

        new cognito.CfnUserPoolGroup(this, 'ParticipantGroup', {
            userPoolId: this.userPool.userPoolId,
            groupName: 'participant',
            precedence: 10,
            description: 'Participants',
        });
    }
}
