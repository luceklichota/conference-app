import {
    aws_s3 as s3,
    aws_s3_deployment as s3deploy,
    aws_cloudfront as cloudfront,
    CfnOutput,
} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {UserPool, UserPoolClient, UserPoolDomain} from "aws-cdk-lib/aws-cognito";

interface ConfigProps {
    hostingBucket: s3.IBucket
    webDistribution: cloudfront.CloudFrontWebDistribution
    userPool: UserPool
    userPoolClient: UserPoolClient
    userPoolDomain: UserPoolDomain
    apiUrl: string
}

export class Config extends Construct {
    constructor(scope: Construct, id: string, props: ConfigProps) {
        super(scope, id);

        const configData = {
                awsUserPoolId: props.userPool.userPoolId,
                awsUserPoolWebClientId: props.userPoolClient.userPoolClientId,
                domain: props.userPoolDomain.baseUrl(),
                baseUrl: `https://${props.webDistribution.distributionDomainName}/`,
                apiUrl: props.apiUrl,
            };

        new s3deploy.BucketDeployment(this, 'DeployWebsite', {
            sources: [s3deploy.Source.data('config.js', `window['appConfig'] = ${JSON.stringify(configData)}`)],
            destinationBucket: props.hostingBucket,
            prune: false
        });

        new CfnOutput(this, 'URL', {
            value: `https://${props.webDistribution.distributionDomainName}/`
        });
    }
}
