import {
  aws_dynamodb as dynamodb
} from 'aws-cdk-lib';
import {Construct} from "constructs";

export class AppDatabase extends Construct {
  public readonly conferenceTable: dynamodb.ITable;

  constructor(scope: Construct, id: string) {
    super(scope, id);

    const conferenceTable = new dynamodb.Table(this, 'DocumentsTable', {
      billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
      partitionKey: {
        name: 'PK',
        type: dynamodb.AttributeType.STRING,
      },
      sortKey: {
        name: 'SK',
        type: dynamodb.AttributeType.STRING,
      },
    });

    conferenceTable.addGlobalSecondaryIndex({
      indexName: 'EventsGSI',
      partitionKey: {
        name: 'SK',
        type: dynamodb.AttributeType.STRING,
      },
      sortKey: {
        name: 'PK',
        type: dynamodb.AttributeType.STRING,
      },
      projectionType: dynamodb.ProjectionType.INCLUDE,
      nonKeyAttributes: ['name', 'startDate', 'endDate', 'address', 'coordinates'],
    });

    conferenceTable.addGlobalSecondaryIndex({
      indexName: 'VenuesGSI',
      partitionKey: {
        name: 'SK',
        type: dynamodb.AttributeType.STRING,
      },
      sortKey: {
        name: 'PK',
        type: dynamodb.AttributeType.STRING,
      },
      projectionType: dynamodb.ProjectionType.INCLUDE,
      nonKeyAttributes: ['name', 'startDate', 'endDate', 'address', 'coordinates', 'limit'],
    });

    this.conferenceTable = conferenceTable;
  }
}
