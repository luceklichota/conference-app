import {Stack, StackProps} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {WebApp} from "./webapp";
import 'dotenv/config'
import {Auth} from "./auth";
import {Config} from "./config";
import {AppDatabase} from "./database";
import {AppServices} from "./services";
import {ApplicationAPI} from "./api";
import {AppSqlDatabase} from "./sql_database";
import {ApplicationNetworking} from "./networking";
import {ApplicationSecrets} from "./secrets";


export class ApplicationStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        const networking = new ApplicationNetworking(this, 'Networking')
        const secrets = new ApplicationSecrets(this, 'Secrets')
        const database = new AppDatabase(this, 'Database');
        const sqlDatabase = new AppSqlDatabase(this, 'SqlDatabase', {
            vpc: networking.vpc,
            secret: secrets.db
        });

        const services = new AppServices(this, 'Services', {
            db: database.conferenceTable,
            vpc: networking.vpc,
            dbCredentials: secrets.db
        })

        const webapp = new WebApp(this, 'WebApp');

        const auth = new Auth(this, 'Auth', {
            googleId: <string>process.env.GOOGLE_CLIENT_ID,
            googleSecret: <string>process.env.GOOGLE_CLIENT_SECRET,
            webDistribution: webapp.webDistribution
        })

        const api = new ApplicationAPI(this, 'API', {
            cityService: services.cityService,
            venuesService: services.venuesService,
            eventsService: services.eventsService,
            userService: services.userService,
            userPool: auth.userPool,
            userPoolClient: auth.client
        })

        new Config(this, 'Config', {
            hostingBucket: webapp.hostingBucket,
            webDistribution: webapp.webDistribution,
            userPool: auth.userPool,
            userPoolClient: auth.client,
            userPoolDomain: auth.domain,
            apiUrl: api.httpApi.apiEndpoint
        })
    }
}
