import {
    aws_ec2 as ec2
} from 'aws-cdk-lib';

import {Construct} from "constructs";
import {Vpc} from "aws-cdk-lib/aws-ec2";


export class ApplicationNetworking extends Construct {
    public readonly vpc: Vpc;

    constructor(scope: Construct, id: string) {
        super(scope, id);

        this.vpc = new Vpc(this, "DbVPC", {
            natGateways: 0,
            subnetConfiguration: [
                {
                    name: 'public-subnet-1',
                    subnetType: ec2.SubnetType.PUBLIC,
                    cidrMask: 24,
                },
            ]
        })


    }
}
