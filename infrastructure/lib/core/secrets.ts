import {
    aws_secretsmanager as secretsmanager,
} from 'aws-cdk-lib';

import {Construct} from "constructs";

export class ApplicationSecrets extends Construct {
    public readonly db: secretsmanager.Secret;

    constructor(scope: Construct, id: string) {
        super(scope, id);

        this.db = new secretsmanager.Secret(this, 'DB', {
            secretName: 'mysql',
            generateSecretString: {
                secretStringTemplate: JSON.stringify({ username: 'user' }),
                generateStringKey: 'password',
                excludeCharacters: "% ^@%+~`$&*()|[]{}:;,-<>?!'/\\\",="
            }
        })
    }
}
