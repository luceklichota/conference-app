import * as path from 'path';
import {
    aws_dynamodb as dynamodb,
    aws_iam as iam
} from 'aws-cdk-lib';
import {Construct} from "constructs";
import {NodejsFunction} from "aws-cdk-lib/aws-lambda-nodejs";
import {NodejsServiceFunction} from "../constructs/lambda";
import {Vpc} from "aws-cdk-lib/aws-ec2";
import {Secret} from "aws-cdk-lib/aws-secretsmanager";

interface AppServicesProps {
    db: dynamodb.ITable,
    vpc: Vpc,
    dbCredentials: Secret
}

export class AppServices extends Construct {
    public readonly cityService: NodejsFunction;
    public readonly venuesService: NodejsFunction;
    public readonly eventsService: NodejsFunction;
    public readonly userService: NodejsFunction;

    constructor(scope: Construct, id: string, props: AppServicesProps) {
        super(scope, id);

        this.cityService = new NodejsServiceFunction(this, 'CityServiceLambda', {
            entry: path.join(__dirname, '../../../services/city/index.js')
        });
        props.dbCredentials.grantRead(this.cityService);

        this.cityService.addToRolePolicy(
            new iam.PolicyStatement({
                resources: ['*'],
                actions: [
                    "cognito-idp:AddCustomAttributes",
                    "cognito-idp:UpdateUserAttributes",
                    "cognito-idp:AdminUpdateUserAttributes"
                ],
            }),
        );

        this.venuesService = new NodejsServiceFunction(this, 'VenuesServiceLambda', {
            entry: path.join(__dirname, '../../../services/venues/index.js'),
        });

        props.db.grantReadWriteData(this.venuesService);

        this.venuesService.addEnvironment('DYNAMO_DB_TABLE', props.db.tableName);

        this.venuesService.addToRolePolicy(
            new iam.PolicyStatement({
                resources: ['*'],
                actions: [
                    "cognito-idp:AdminGetUser"
                ],
            }),
        );
        props.dbCredentials.grantRead(this.venuesService);

        this.eventsService = new NodejsServiceFunction(this, 'EventsServiceLambda', {
            entry: path.join(__dirname, '../../../services/events/index.js'),
        });

        this.eventsService.addToRolePolicy(
            new iam.PolicyStatement({
                resources: ['*'],
                actions: [
                    "cognito-idp:AdminGetUser"
                ],
            }),
        );

        props.db.grantReadWriteData(this.eventsService);
        props.dbCredentials.grantRead(this.eventsService);

        this.userService = new NodejsServiceFunction(this, 'UserServiceLambda', {
            entry: path.join(__dirname, '../../../services/user/index.js'),
        });

        this.userService.addToRolePolicy(
            new iam.PolicyStatement({
                resources: ['*'],
                actions: [
                    "cognito-idp:AdminGetUser"
                ],
            }),
        );
    }
}
