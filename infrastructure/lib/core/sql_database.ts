import {
    aws_rds as rds,
    aws_ec2 as ec2,
    aws_secretsmanager as secretsmanager
} from 'aws-cdk-lib';
import {Construct} from "constructs";
import {Peer, Port, SecurityGroup, Vpc} from "aws-cdk-lib/aws-ec2";

interface SqlDatabaseProps {
    vpc: Vpc,
    secret: secretsmanager.Secret
}

export class AppSqlDatabase extends Construct {

    constructor(scope: Construct, id: string, props: SqlDatabaseProps) {
        super(scope, id);
        const engine = rds.DatabaseInstanceEngine.mysql({ version: rds.MysqlEngineVersion.VER_8_0_28 });
        const mySecret = secretsmanager.Secret.fromSecretNameV2(this, 'DB', props.secret.secretName);

        const sg = new SecurityGroup(this, `RDS`, {
            vpc: props.vpc,
            allowAllOutbound: true,
            description: "RDS SG",
        });

        sg.addIngressRule(Peer.anyIpv4(), Port.tcp(3306), "DB");

        const subnets = props.vpc.selectSubnets({
            subnetType: ec2.SubnetType.PUBLIC
        });

        const db = new rds.DatabaseInstance(this, 'mysql', {
            engine,
            vpc: props.vpc,
            credentials: rds.Credentials.fromSecret(mySecret),
            publiclyAccessible: true,
            instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
            databaseName: 'conference',
            securityGroups: [
                sg
            ],
            vpcSubnets: subnets
        });
    }
}
