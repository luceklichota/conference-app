import {
    aws_s3 as s3,
    DockerImage,
    aws_cloudfront as cloudfront,
} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import * as cwt from 'cdk-webapp-tools';


export class WebApp extends Construct {
    public readonly webDistribution: cloudfront.CloudFrontWebDistribution;
    public readonly hostingBucket: s3.IBucket;

    constructor(scope: Construct, id: string) {
        super(scope, id);

        this.hostingBucket = new s3.Bucket(this, 'WebHostingBucket', {
            encryption: s3.BucketEncryption.S3_MANAGED,
        });

        const oai = new cloudfront.OriginAccessIdentity(this, 'WebHostingOAI', {});

        const cloudfrontProps: any = {
            originConfigs: [
                {
                    s3OriginSource: {
                        s3BucketSource: this.hostingBucket,
                        originAccessIdentity: oai,
                    },
                    behaviors: [{isDefaultBehavior: true}],
                },
            ],
            errorConfigurations: [
                {
                    errorCachingMinTtl: 86400,
                    errorCode: 403,
                    responseCode: 200,
                    responsePagePath: '/index.html',
                },
                {
                    errorCachingMinTtl: 86400,
                    errorCode: 404,
                    responseCode: 200,
                    responsePagePath: '/index.html',
                },
            ],
        };

        this.webDistribution = new cloudfront.CloudFrontWebDistribution(
            this,
            'AppHostingDistribution',
            cloudfrontProps,
        );

        this.hostingBucket.grantRead(oai);

        //TODO: get rid of this package, deploy with s3 deployment
        new cwt.WebAppDeployment(this, 'WebAppDeploy', {
            baseDirectory: '../',
            relativeWebAppPath: 'webapp',
            buildCommand: 'yarn build',
            buildDirectory: 'dist/conference',
            bucket: this.hostingBucket,
            prune: false,
            dockerImage: new DockerImage('node:16')
        });
    }
}
