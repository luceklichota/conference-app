import {
    createRouter,
    RouterType,
    Matcher,
    validateBodyJSONVariables,
} from 'lambda-micro';
import {AWSClients} from '../common';

const schemas = {
    changeCity: require('./schemas/changeCity.json'),
};

const cognitoIdentityServiceProvider = AWSClients.cognitoIdentityServiceProvider();

const setCityForUser = async (request, response) => {
    const iss = request.event.requestContext.authorizer.jwt.claims.iss
    const poolId = iss.substring(iss.lastIndexOf("/") + 1);
    const username = request.event.requestContext.authorizer.jwt.claims.username

    const body = JSON.parse(request.event.body)
    const city = body.city

    try {
        await cognitoIdentityServiceProvider.adminUpdateUserAttributes(
            {
                UserAttributes: [
                    {
                        Name: 'custom:city',
                        Value: city
                    }
                ],
                UserPoolId: poolId,
                Username: username
            },
            function (error) {
                response.output(error, 400);
            }
        ).promise()
    } catch (error) {
        response.output(error, 400);
    }

    return response.output({city}, 200);
};

const getCities = async (request, response) => {
    const cities = [
        'Warszawa',
        'Lublin',
        'Poznań',
        'Białystok',
        'Kielce',
        'Zdalnie'
    ]
    return response.output(cities, 200);
};

const router = createRouter(RouterType.HTTP_API_V2);

router.add(
    Matcher.HttpApiV2('GET', '/cities/'),
    getCities,
);

router.add(
    Matcher.HttpApiV2('POST', '/user-city/'),
    validateBodyJSONVariables(schemas.changeCity),
    setCityForUser,
);

exports.handler = async (event, context) => {
    return router.run(event, context);
};
