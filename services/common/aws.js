import * as AWS from 'aws-sdk';

let _dynamoDB;
let _cognitoIdentityServiceProvider

const dynamoDB = () => {
  if (!_dynamoDB) {
    _dynamoDB = new AWS.DynamoDB.DocumentClient();
  }
  return _dynamoDB;
};

const cognitoIdentityServiceProvider = () => {
  if (!_cognitoIdentityServiceProvider) {
    _cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider;
  }
  return _cognitoIdentityServiceProvider;
};


export const AWSClients = {
  dynamoDB,
  cognitoIdentityServiceProvider
};
