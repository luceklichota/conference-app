const mysql = require("mysql");
const AWS = require('aws-sdk'),
    region = "eu-central-1",
    secretName = "mysql"

const client = new AWS.SecretsManager({
    region: region
});

export const mysqlConnection = async () => {
    let secretValue = await client.getSecretValue({SecretId: secretName}).promise();
    const credentials = JSON.parse(secretValue.SecretString)

    return mysql.createConnection({
        host: credentials.host,
        user: credentials.username,
        password: credentials.password,
        port: credentials.port,
        database: credentials.dbname,
    });
}
