import KSUID from 'ksuid';

export const generateID = () => {
  return KSUID.randomSync().string;
};

export const isValidDate = (date) => {
  let isValidDate = Date.parse(date)

  return !isNaN(isValidDate)
}

export const apiError = (error) => {
  return {
    error: true,
    message: error
  };
}


export const apiSuccess = (message) => {
  return {
    error: false,
    message: message
  };
}
