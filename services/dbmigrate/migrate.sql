create table venues
(
    id          varchar(30) not null,
    name        text        null,
    startDate   datetime    null,
    endDate     datetime    null,
    address     text        null,
    coordinates text        null,
    description text        null,
    ownerId    text        not null,
    `limit`     int         null,
    creationDate     datetime         default NOW(),
    constraint venues_pk
        primary key (id)
);

create unique index venues_id_uindex
    on venues (id);


create table events
(
    id          varchar(30) not null,
    venueID     varchar(30) not null,
    name        text        null,
    startDate   datetime    null,
    endDate     datetime    null,
    place       int         null,
    description int         null,
    ownerId    text        not null,
    `limit`     int         null,
    creationDate     datetime         default NOW(),
    constraint events_pk
        primary key (id)
);

create unique index events_id_uindex
    on events (id);

create table venue_attendees
(
    venueId    varchar(30)            not null,
    userId     varchar(128)           not null,
    userEmail  varchar(128)           not null,
    attendDate datetime default NOW() not null
);

create table event_attendees
(
    eventId    varchar(30)            not null,
    userId     varchar(128)           not null,
    userEmail  varchar(128)           not null,
    attendDate datetime default NOW() not null
);

create table labels
(
    id    int auto_increment,
    label varchar(50) not null,
    color varchar(6)  null,
    constraint labels_pk
        primary key (id)
);

create table events_label
(
    eventId varchar(30) not null,
    labelId int         not null
);

alter table events
    add presenter varchar(30) null after ownerId;

alter table events
    modify creationDate datetime default CURRENT_TIMESTAMP null after presenter;

create table event_rating
(
    eventId varchar(30) not null,
    userId  varchar(30) not null,
    rating  int         not null,
    userEmail varchar(30) null
);

alter table labels
    modify color varchar(8) not null;
