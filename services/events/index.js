import {
    createRouter,
    RouterType,
    Matcher,
    validateBodyJSONVariables, enforceGroupMembership, validatePathVariables,
} from 'lambda-micro';
import {apiError, apiSuccess, AWSClients, generateID, isValidDate} from '../common';
import {mysqlConnection} from "@conference/services-common/db";

const schemas = {
    createEvent: require('./schemas/createEvent.json'),
    attendEvent: require('./schemas/attendEvent.json'),
};

const cognitoIdentityServiceProvider = AWSClients.cognitoIdentityServiceProvider();

const createEvent = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const eventId = generateID()
    const body = JSON.parse(request.event.body);

    let itemFields = {...body}

    if (!isValidDate(itemFields.startDate)) {
        return response.output(apiError('Invalid start date'), 400);
    }
    const startDate = new Date(itemFields.startDate)
    itemFields.startDate = startDate.toJSON().slice(0, 19).replace('T', ' ');

    if (itemFields.endDate && !isValidDate(itemFields.endDate)) {
        return response.output(apiError('Invalid end date'), 400);
    }

    const endDate = new Date(itemFields.endDate)
    itemFields.endDate = endDate.toJSON().slice(0, 19).replace('T', ' ');

    if(endDate.getTime() < startDate.getTime()) {
        return response.output(apiError('End date is before start date'), 400);
    }

    itemFields.id = eventId
    itemFields.ownerId = userId

    delete itemFields.labels

    const con = await mysqlConnection()
    const sql = "INSERT INTO events SET ?";
    const parameters = {
        ...itemFields
    }

    try {
        await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }



    const labels = await getAllLabels()

    let labelsToCreate = []
    let labelsToAdd = []

    console.log(itemFields)

    body.labels.forEach(label => {
        let existingLabel = labels.filter(existingLabel => existingLabel.label === label.label)
        if (existingLabel.length > 0) {
            console.log(existingLabel)
            labelsToAdd.push(existingLabel[0].id)
        } else {
            labelsToCreate.push(label)
        }
    })

    for (const label of labelsToCreate) {
        labelsToAdd.push(await addLabel(label))
    }

    console.log(labelsToAdd)

    for (const label of labelsToAdd) {
        await bindLabel(eventId, label)
    }

    return response.output(itemFields, 200)
};

const bindLabel = async (eventId, labelId) => {
    const con = await mysqlConnection()
    const sql = "INSERT INTO events_label SET ?";
    const parameters = {
        eventId: eventId,
        labelId: labelId
    }

    let result

    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
    }

    return result.insertId
}

const addLabel = async (label) => {
    const con = await mysqlConnection()
    const sql = "INSERT INTO labels SET ?";
    const parameters = {
        label: label.label,
        color: label.color
    }

    let result

    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
    }

    return result.insertId
}

const attendEvent = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const iss = request.event.requestContext.authorizer.jwt.claims.iss
    const poolId = iss.substring(iss.lastIndexOf('/') + 1)
    const user = await cognitoIdentityServiceProvider.adminGetUser({
        UserPoolId: poolId,
        Username: userId,
    }).promise()

    const emailAttributes = user.UserAttributes.filter(attribute => attribute.Name === 'email')

    const con = await mysqlConnection()
    const sql = "SELECT * FROM event_attendees WHERE eventId = ? AND userId = ?";
    const parameters = [request.pathVariables.eventId, userId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    if (result.length > 0) {
        return response.output(apiError('User already attend on this venue'), 200)
    }

    const stats = await countAttendees(request.pathVariables.venueId)

    if (stats.length > 0) {
        if (+stats[0].attendees >= +stats[0].limit) {
            return response.output(apiError('Attendees limit reached'), 400)
        }
    }

    const insertSql = "INSERT INTO event_attendees SET ?";
    const insertParameters = {
        eventId: request.pathVariables.eventId,
        userId: userId,
        userEmail: emailAttributes[0].Value,
    }

    try {
        await new Promise((resolve, reject) => {
            con.query(insertSql, insertParameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    con.end()
    return response.output(result, 200)
};

const unattendEvent = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;

    const con = await mysqlConnection()
    const sql = "DELETE FROM event_attendees WHERE eventId = ? AND userId = ?";
    const parameters = [request.pathVariables.eventId, userId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    return response.output(apiSuccess('Unattended'), 200)
};

const countAttendees = async (eventId) => {
    const con = await mysqlConnection()
    const sql = "SELECT v.limit, v.id, COUNT(a.userId) as attendees FROM events v JOIN event_attendees a ON v.id = a.eventId WHERE v.id = ? GROUP BY a.eventId";
    const parameters = [eventId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        throw e
    }

    return result
}

const getAllLabels = async () => {
    const con = await mysqlConnection()
    const sql = "SELECT * FROM labels";
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        throw e
    }

    return result
}

const rateEvent = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const iss = request.event.requestContext.authorizer.jwt.claims.iss
    const poolId = iss.substring(iss.lastIndexOf('/') + 1)
    const user = await cognitoIdentityServiceProvider.adminGetUser({
        UserPoolId: poolId,
        Username: userId,
    }).promise()

    const emailAttributes = user.UserAttributes.filter(attribute => attribute.Name === 'email')

    const con = await mysqlConnection()
    const sql = "SELECT * FROM event_rating WHERE eventId = ? AND userId = ?";
    const parameters = [request.pathVariables.eventId, userId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    if (result.length > 0) {
        return response.output(apiError('User already rated this event'), 400)
    }

    const stats = await countAttendees(request.pathVariables.venueId)

    if (stats.length > 0) {
        if (+stats[0].attendees >= +stats[0].limit) {
            return response.output(apiError('Attendees limit reached'), 400)
        }
    }

    const body = JSON.parse(request.event.body);

    const insertSql = `INSERT INTO event_rating SET ?`;
    const insertParameters = {
        eventId: request.pathVariables.eventId,
        userId: userId,
        userEmail: emailAttributes[0].Value,
        rating: body.rating,
    }

    try {
        await new Promise((resolve, reject) => {
            con.query(insertSql, insertParameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    con.end()
    return response.output(result, 200)

}

const getTags = async (request, response) => {

    return response.output(await getAllLabels(), 200)
}
const router = createRouter(RouterType.HTTP_API_V2);

router.add(
    Matcher.HttpApiV2('POST', '/events/'),
    validateBodyJSONVariables(schemas.createEvent),
    enforceGroupMembership('admin'),
    createEvent,
);

router.add(
    Matcher.HttpApiV2('POST', '/events/attend/(:eventId)'),
    validatePathVariables(schemas.attendEvent),
    attendEvent,
);

router.add(
    Matcher.HttpApiV2('POST', '/events/unattend/(:eventId)'),
    validatePathVariables(schemas.attendEvent),
    unattendEvent,
);

router.add(
    Matcher.HttpApiV2('POST', '/events/rate/(:eventId)'),
    validatePathVariables(schemas.attendEvent),
    rateEvent,
);

router.add(
    Matcher.HttpApiV2('GET', '/events/tags/'),
    getTags,
);


exports.handler = async (event, context) => {
    return router.run(event, context);
};
