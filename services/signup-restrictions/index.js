exports.handler = (event, context, callback) => {
    console.log ("Trigger function =", event.triggerSource);

    // Send post authentication data to Cloudwatch logs
    if (event.request.userAttributes.email.endsWith('@britenet.com.pl')) {
        console.log ("Authentication successful: ", event.request);
        callback(null, event);
    } else {
        console.log ("Authentication failed: ", event.request);
        callback("Email not from domain", event)
    }
};
