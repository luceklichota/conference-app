import {
    createRouter,
    RouterType,
    Matcher,
    validatePathVariables,
} from 'lambda-micro';
import {AWSClients} from '../common';


const cognitoIdentityServiceProvider = AWSClients.cognitoIdentityServiceProvider();

const userDetails = async (request, response) => {
    return response.output(request.event.requestContext.authorizer.jwt.claims, 200)
}


const router = createRouter(RouterType.HTTP_API_V2);

router.add(
    Matcher.HttpApiV2('GET', '/user/details/'),
    userDetails,
);


exports.handler = async (event, context) => {
    return router.run(event, context);
};
