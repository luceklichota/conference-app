import {
    createRouter,
    RouterType,
    Matcher,
    validateBodyJSONVariables, enforceGroupMembership, validatePathVariables,
} from 'lambda-micro';
import {apiError, apiSuccess, AWSClients, generateID, isValidDate} from '../common';
import {mysqlConnection} from "@conference/services-common/db";


// JSON schemas used to validate requests to the service calls
const schemas = {
    createVenue: require('./schemas/createVenue.json'),
    deleteVenue: require('./schemas/deleteVenue.json'),
    getVenue: require('./schemas/getVenue.json'),
    attendVenue: require('./schemas/attendVenue.json'),
};

const cognitoIdentityServiceProvider = AWSClients.cognitoIdentityServiceProvider();

const createVenue = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const venueId = generateID();
    const body = JSON.parse(request.event.body);

    let itemFields = {...body}

    if (!isValidDate(itemFields.startDate)) {
        return response.output(apiError('Invalid start date'), 400);
    }
    const startDate = new Date(itemFields.startDate)
    itemFields.startDate = startDate.toJSON().slice(0, 19).replace('T', ' ');


    if (itemFields.endDate && !isValidDate(itemFields.endDate)) {
        return response.output(apiError('Invalid end date'), 400);
    }

    const endDate = new Date(itemFields.endDate)
    itemFields.endDate = endDate.toJSON().slice(0, 19).replace('T', ' ');

    if (endDate.getTime() < startDate.getTime()) {
        return response.output(apiError('End date is before start date'), 400);
    }

    itemFields.id = venueId
    itemFields.ownerId = userId

    const con = await mysqlConnection()
    const sql = "INSERT INTO venues SET ?";
    const parameters = {
        ...itemFields
    }

    try {
        await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    return response.output(itemFields, 200)
};

const getVenue = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const con = await mysqlConnection()
    const sql = `
        SELECT v.*, a.attendDate, count(ac.userId) as attendeesCount
        FROM venues v
                 LEFT JOIN venue_attendees a ON v.id = a.venueId AND a.userId = ?
                 LEFT JOIN venue_attendees ac ON v.id = ac.venueId
        WHERE v.id = ?
        GROUP BY ac.venueId`;
    const parameters = [userId, request.pathVariables.venueId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    const output = result.map((venue => {
        venue.isAttending = false
        venue.canAttend = true
        if (venue.attendDate) {
            venue.isAttending = true
        }

        if (+venue.limit <= +venue.attendeesCount) {
            venue.canAttend = false
        }

        delete venue.attendDate

        return venue
    }))

    return response.output(output[0], 200)
}

const getVenueEvents = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const con = await mysqlConnection()
    const sql = `
        SELECT e.*, a.attendDate, COUNT(ac.userId) as attendeesCount
        FROM events e
                 LEFT JOIN event_attendees a ON e.id = a.eventId AND a.userId = ?
                 LEFT JOIN event_attendees ac ON e.id = ac.eventId
        WHERE e.venueID = ?
        GROUP BY e.id`;
    const parameters = [userId, request.pathVariables.venueId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    const output = result.map((event => {
        event.isAttending = false
        event.canAttend = true
        if (event.attendDate) {
            event.isAttending = true
        }

        if (+event.limit <= +event.attendeesCount) {
            event.canAttend = false
        }

        delete event.attendDate

        return event
    }))

    return response.output(output, 200)
}

const getAllVenues = async (request, response) => {
    const con = await mysqlConnection()
    const userId = request.event.requestContext.authorizer.jwt.claims.username;

    const sql = `
        SELECT v.*, a.attendDate, count(ac.userId) as attendeesCount
        FROM venues v
                 LEFT JOIN venue_attendees a ON v.id = a.venueId AND a.userId = ?
                 LEFT JOIN venue_attendees ac ON v.id = ac.venueId
        GROUP BY v.id`;

    const parameters = [userId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    const output = result.map((venue => {
        venue.isAttending = false
        venue.canAttend = true
        if (venue.attendDate) {
            venue.isAttending = true
        }

        if (+venue.limit <= +venue.attendeesCount) {
            venue.canAttend = false
        }

        delete venue.attendDate

        return venue
    }))

    return response.output(output, 200)
};

const myVenues = async (request, response) => {
    console.log('my')
    const con = await mysqlConnection()
    const userId = request.event.requestContext.authorizer.jwt.claims.username;

    const sql = `
        SELECT v.*, a.attendDate, count(ac.userId) as attendeesCount
        FROM venues v
                 JOIN venue_attendees a ON v.id = a.venueId AND a.userId = ?
                 LEFT JOIN venue_attendees ac ON v.id = ac.venueId
        GROUP BY v.id`;

    const parameters = [userId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    const eventsSql = `
        SELECT e.*, a.attendDate, r.rating, COUNT(ac.userId) as attendeesCount
        FROM events e
                 JOIN event_attendees a ON e.id = a.eventId AND a.userId = ?
                 LEFT JOIN event_attendees ac ON e.id = ac.eventId
                 LEFT JOIN event_rating r ON e.id = r.eventId AND r.userId = ?
        
        GROUP BY e.id, e.startDate
        ORDER BY e.startDate`;
    const eventsParameters = [userId, userId]

    let eventsResult
    try {
        eventsResult = await new Promise((resolve, reject) => {
            con.query(eventsSql, eventsParameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    const events = eventsResult.map(event => {
        event.isAttending = true;
        event.isRated = false;
        if (event.rating) {
            event.isRated = true
        }
    })

    const output = result.map(venue => {
        venue.isAttending = false
        venue.canAttend = true
        if (venue.attendDate) {
            venue.isAttending = true
        }

        if (+venue.limit <= +venue.attendeesCount) {
            venue.canAttend = false
        }

        venue.events = events.filter(event => {
            console.log(event)
            console.log(event.venueID)
            console.log(venue.id)
            return event.venueID === venue.id
        })

        delete venue.attendDate

        return venue
    })

    console.log(output)

    return response.output(output, 200)
};

const deleteVenue = async (request, response) => {
    //TODO: deleting related object
    const con = await mysqlConnection()
    const sql = "DELETE FROM venues WHERE id = ?";
    const parameters = [request.pathVariables.venueId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    return response.output(result[0], 200)
};

const attendVenue = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;
    const iss = request.event.requestContext.authorizer.jwt.claims.iss
    const poolId = iss.substring(iss.lastIndexOf('/') + 1)
    const user = await cognitoIdentityServiceProvider.adminGetUser({
        UserPoolId: poolId,
        Username: userId,
    }).promise()

    const emailAttributes = user.UserAttributes.filter(attribute => attribute.Name === 'email')

    const con = await mysqlConnection()
    const sql = "SELECT * FROM venue_attendees WHERE venueId = ? AND userId = ?";
    const parameters = [request.pathVariables.venueId, userId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    if (result.length > 0) {
        return response.output(apiError('User already attend on this venue'), 400)
    }

    const stats = await countAttendees(request.pathVariables.venueId)

    if (stats.length > 0) {
        if (+stats[0].attendees >= +stats[0].limit) {
            return response.output(apiError('Attendees limit reached'), 400)
        }
    }

    const insertSql = "INSERT INTO venue_attendees SET ?";
    const insertParameters = {
        venueId: request.pathVariables.venueId,
        userId: userId,
        userEmail: emailAttributes[0].Value,
    }

    try {
        await new Promise((resolve, reject) => {
            con.query(insertSql, insertParameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    con.end()
    return response.output(result, 200)
};

const unattendVenue = async (request, response) => {
    const userId = request.event.requestContext.authorizer.jwt.claims.username;

    const con = await mysqlConnection()
    const sql = "DELETE FROM venue_attendees WHERE venueId = ? AND userId = ?";
    const parameters = [request.pathVariables.venueId, userId]
    let result
    try {
        await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
    } catch (e) {
        console.error(e)
        con.destroy()
        return response.output(apiError('DB error'), 200)
    }

    return response.output(apiSuccess('Unattended'), 200)
};

const countAttendees = async (venueId) => {
    const con = await mysqlConnection()
    const sql = "SELECT v.limit, v.id, COUNT(a.userId) as attendees FROM venues v JOIN venue_attendees a ON v.id = a.venueId WHERE v.id = ? GROUP BY a.venueId";
    const parameters = [venueId]
    let result
    try {
        result = await new Promise((resolve, reject) => {
            con.query(sql, parameters, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        });
        con.end()
    } catch (e) {
        console.error(e)
        con.destroy()
        throw e
    }

    return result
}


const router = createRouter(RouterType.HTTP_API_V2);

router.add(
    Matcher.HttpApiV2('GET', '/venues/'),
    getAllVenues,
);

router.add(
    Matcher.HttpApiV2('GET', '/venues/(:venueId)'),
    validatePathVariables(schemas.getVenue),
    getVenue,
);

router.add(
    Matcher.HttpApiV2('GET', '/venues/(:venueId)/events'),
    validatePathVariables(schemas.getVenue),
    getVenueEvents,
);

router.add(
    Matcher.HttpApiV2('POST', '/venues/'),
    validateBodyJSONVariables(schemas.createVenue),
    enforceGroupMembership('admin'),
    createVenue,
);

router.add(
    Matcher.HttpApiV2('DELETE', '/venues/(:venueId)'),
    enforceGroupMembership('admin'),
    validatePathVariables(schemas.deleteVenue),
    deleteVenue,
);

router.add(
    Matcher.HttpApiV2('POST', '/venues/attend/(:venueId)'),
    validatePathVariables(schemas.attendVenue),
    attendVenue,
);

router.add(
    Matcher.HttpApiV2('POST', '/venues/unattend/(:venueId)'),
    validatePathVariables(schemas.attendVenue),
    unattendVenue,
);

router.add(
    Matcher.HttpApiV2('GET', '/venues/my/'),
    myVenues,
);

exports.handler = async (event, context) => {
    return router.run(event, context);
};
