import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminContainerComponent } from './admin-container/admin-container.component';
import { RouterModule, Routes } from "@angular/router";
import { UiDomainModule } from "../ui-domain/ui-domain.module";
import { VenueCreateComponent } from './event-create/venue-create.component';
import { LectureCreateComponent } from './lecture-create/lecture-create.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EventListComponent } from './venue-list/event-list.component';
import { LecturerListComponent } from './lecturer-list/lecturer-list.component';

const adminRoutes: Routes = [{
  path: '', component: AdminContainerComponent, children: [
    { path: '', pathMatch: 'full', redirectTo: 'admin-dashboard' },
    { path: 'admin-dashboard', component: AdminDashboardComponent, canActivate: [] },
    { path: 'create-lecture', component: LectureCreateComponent, canActivate: [] },
    { path: 'create-event', component: VenueCreateComponent, canActivate: [] },
    { path: 'venue-list', component: EventListComponent, canActivate: [] },
    { path: 'event-list', component: LecturerListComponent, canActivate: [] },
  ]
}]


@NgModule({
  declarations: [
    AdminContainerComponent,
    VenueCreateComponent,
    LectureCreateComponent,
    AdminDashboardComponent,
    EventListComponent,
    LecturerListComponent
  ],
  imports: [
    CommonModule,
    UiDomainModule,
    RouterModule.forChild(adminRoutes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
