import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { AdminService } from "../services/admin.service";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-venue-create',
  templateUrl: './venue-create.component.html',
  styleUrls: ['./venue-create.component.scss']
})
export class VenueCreateComponent implements OnInit {
  form: FormGroup = new FormGroup({});

  constructor(private fb: FormBuilder, private venueService: AdminService, private router: Router,private messageService: MessageService) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.form = this.fb.group({
      name: [''],
      startDate: [''],
      endDate: [''],
      address: [''],
      description: [''],
      limit: [null]
    })
  }

  public createEvent(): void {
    this.venueService.createVenue(this.form.getRawValue())
      .subscribe((data) => {
        this.router.navigate(['admin']);
        this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Dodałeś event', life: 3000});
      })
  }
}
