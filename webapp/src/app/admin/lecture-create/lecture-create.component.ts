import { Component, OnInit } from '@angular/core';
import { EventService, VenuesResponse } from "../../event-box/components/services/event.service";
import { AdminService } from "../services/admin.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-lecture-create',
  templateUrl: './lecture-create.component.html',
  styleUrls: ['./lecture-create.component.scss']
})
export class LectureCreateComponent implements OnInit {
  public venues: VenuesResponse[];
  public tags = [
    {label: 'UX', color: '#2A58FE'},
    {label: 'Frontend', color: '#2a8dfe'},
    {label: 'Backend', color: '#c93678'},
  ]
  public form: FormGroup

  filteredTags: any[];
  constructor(private eventService: EventService, private adminService: AdminService, private formBuilder: FormBuilder, private router: Router, private messageService: MessageService) { }

  ngOnInit(): void {
    this.eventService.getVenues()
      .subscribe((venues) => {
        this.venues = venues;
      })

    this.form = this.formBuilder.group({
      name: [''],
      presenter: [''],
      venueId: [''],
      startDate: [''],
      endDate: [''],
      place: [''],
      description: [''],
      limit: [''],
      labels: ['']
    })

    // this.adminService.getTags()
    //   .subscribe((data) => {
    //     this.tags = data;
    //
    //   })
  }

  filterCountry(event: any) {
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.tags.length; i++) {
      let list = this.tags[i];
      if (list.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(list);
      }
    }

    this.filteredTags = filtered;
  }

  saveEvent(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.adminService.createEvent(this.form.getRawValue())
      .subscribe(() => {
        this.router.navigate(['admin']);
        this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Dodałeś wydarzenie', life: 3000});
      })
  }

}
