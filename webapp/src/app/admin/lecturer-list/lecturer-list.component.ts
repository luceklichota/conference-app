import { Component, OnInit } from '@angular/core';
import { EventService } from "../../event-box/components/services/event.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-lecturer-list',
  templateUrl: './lecturer-list.component.html',
  styleUrls: ['./lecturer-list.component.scss']
})
export class LecturerListComponent implements OnInit {
  isLoading = false;
  events: any[]

  constructor(private eventService: EventService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.getEvents();
  }

  deleteEvent(id: string): void {
    this.eventService.deleteEvent(id)
      .subscribe(() => {
        this.messageService.add({key: 'tc', severity:'success', summary: 'Sukces', detail: 'Poprawnie usunięto wydarzenie', life: 3000});
        this.getEvents()
      })
  }

  private getEvents(): void {
    this.isLoading = true;
    this.eventService.getEvents()
      .subscribe((events) => {
        this.events = events
        this.isLoading = false;
      })
  }
}
