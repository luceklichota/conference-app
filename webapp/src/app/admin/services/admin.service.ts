import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  constructor(private http: HttpClient) {
  }

  public createVenue(venuFormData: any): Observable<{}> {
    venuFormData.limit = +venuFormData.limit
    const payload = {
      ...venuFormData,
      limit: +venuFormData.limit
    }
    return this.http.post<{}>(`${window.appConfig.apiUrl}/venues/`, payload)
  }

  public createEvent(eventForm: any): Observable<any> {
    eventForm.limit = +eventForm.limit
    const payload = {
      ...eventForm,
      limit: +eventForm.limit
    }
    return this.http.post(`${window.appConfig.apiUrl}/events/`, payload)
  }

  public getTags(): Observable<any> {
    return this.http.get(`${window.appConfig.apiUrl}/events/tags/`);
  }
}
