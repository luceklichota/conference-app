import { Component, OnInit } from '@angular/core';
import { EventService, VenuesResponse } from "../../event-box/components/services/event.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-venue-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {
  isLoading = false;
  venues: VenuesResponse[]

  constructor(private eventService: EventService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.getVenues();
  }

  deleteEvent(id: string): void {
    this.eventService.deleteVenue(id)
      .subscribe(() => {
        this.messageService.add({key: 'tc', severity:'success', summary: 'Sukces', detail: 'Poprawnie usunięto event', life: 3000});
        this.getVenues();
      })
  }

  private getVenues(): void {
    this.isLoading = true;
    this.eventService.getVenues()
      .subscribe((venues) => {
        this.venues = venues
        this.isLoading = false;
      })
  }
}
