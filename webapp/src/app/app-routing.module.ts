import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./authorization/components/login/login.component";
import { SelectRegionComponent } from "./authorization/components/select-region/select-region.component";
import { TokenComponent } from "./authorization/components/token/token.component";
import { SelectRegionGuardService } from "./authorization/guards/select-region-guard.service";

const enum MainRoutes {
  LOGIN = 'login',
  SELECT_REGION = 'select-region',
  PANEL = 'panel',
  TOKEN = 'token',
  OTHERWISE = '**',
  ADMIN = 'admin'
}

const routes: Routes = [
  {
    path: MainRoutes.LOGIN,
    component: LoginComponent
  },
  {
    path: MainRoutes.SELECT_REGION,
    component: SelectRegionComponent,
    canActivate: [SelectRegionGuardService]
  },
  {
    path: MainRoutes.TOKEN,
    component: TokenComponent
  },
  {
    path: MainRoutes.PANEL,
    loadChildren: () => import('./event-box/event-box.module').then((m) => m.EventBoxModule),
  },
  {
    path: MainRoutes.ADMIN,
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
  },
  { path: MainRoutes.OTHERWISE, redirectTo: MainRoutes.PANEL }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [

  ]
})
export class AppRoutingModule {
}
