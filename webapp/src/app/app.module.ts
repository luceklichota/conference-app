import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { UiDomainModule } from "./ui-domain/ui-domain.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthorizationModule } from "./authorization/authorization.module";
import { EventBoxModule } from "./event-box/event-box.module";

import { TokenComponent } from './authorization/components/token/token.component';
import { AuthService } from "./authorization/services/auth.service";
import { MessageService } from "primeng/api";

@NgModule({
  declarations: [
    AppComponent,
    TokenComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    UiDomainModule,
    FormsModule,
    ReactiveFormsModule,
    AuthorizationModule,
    EventBoxModule,
    HttpClientModule
  ],
  providers: [AuthService, MessageService, {
    provide: APP_INITIALIZER,
    useFactory: (authService: AuthService) => () => authService.loadToken(),
    deps: [AuthService],
    multi: true
  }],
  bootstrap: [AppComponent],
})
export class AppModule {
}
