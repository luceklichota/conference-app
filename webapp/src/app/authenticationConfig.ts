const authenticationConfiguration = {
  "aws_user_pools_id": window.appConfig.awsUserPoolId,
  "aws_user_pools_web_client_id": window.appConfig.awsUserPoolWebClientId,
  "oauth": {
    "domain": window.appConfig.domain.replace(/^https?:\/\//, ''),
    "scope": [
      "phone",
      "email",
      "openid",
      "profile",
      "aws.cognito.signin.user.admin"
    ],
    "redirectSignIn": window.appConfig.baseUrl,
    "redirectSignOut": window.appConfig.baseUrl,
    "responseType": "code"
  },
  "aws_cognito_social_providers": [
    "GOOGLE"
  ],
};

export default authenticationConfiguration
