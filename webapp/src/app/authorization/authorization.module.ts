import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { LoginComponent } from './components/login/login.component';
import { UiDomainModule } from "../ui-domain/ui-domain.module";
import { SelectRegionComponent } from './components/select-region/select-region.component';
import { RouterModule } from "@angular/router";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpInterceptorService } from "./interceptors/http-interceptor.service";

@NgModule({
  declarations: [
    LoginComponent,
    SelectRegionComponent
  ],
  imports: [
    CommonModule,
    InputTextModule,
    UiDomainModule,
    RouterModule
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ]
})
export class AuthorizationModule { }
