import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MessageService } from "primeng/api";
import { RegionService } from "../../services/region-service.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs";

@Component({
  selector: 'app-select-region',
  templateUrl: './select-region.component.html',
  styleUrls: ['./select-region.component.scss'],
  providers: [MessageService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectRegionComponent {
  public selectedRegion: string | null = null;
  public isLoading: boolean = false;
  public regions$: Observable<string[]>;

  constructor(
    private messageService: MessageService,
    public regionService: RegionService,
    private router: Router
  ) {
    this.regions$ = this.regionService.getRegions();
  }

  public selectRegion(city: string) {
    this.regionService.saveUserRegion(city)
      .subscribe(() => {
        this.router.navigate(['panel'])
      })
  }
}
