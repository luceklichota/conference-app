import { Component } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { concatMap, filter, tap } from "rxjs";

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.scss']
})
export class TokenComponent {
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.listenOnTokenReceived();
  }

  private listenOnTokenReceived(): void {
    this.authService.tokenReceived$
      .pipe(
        filter((hasToken: boolean) => hasToken),
        concatMap(() => this.authService.loadUserProfile()),
        tap((userInfo) => userInfo && this.router.navigate(['select-region']))
      )
      .subscribe();
  }
}
