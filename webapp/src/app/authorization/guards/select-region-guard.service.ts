import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { map, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SelectRegionGuardService implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {
  }

  canActivate(): Observable<boolean> {
    return this.authService.loadUserProfile().pipe(map((profile) => {
      if (this.authService.getAccessToken() && !profile["custom:city"]) {
        return true
      } else {
        this.router.navigate(['panel'])
        return false
      }
    }))
  }
}
