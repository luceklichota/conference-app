import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { AuthService } from "../services/auth.service";
import { catchError, Observable, throwError } from "rxjs";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getAccessToken();


    if (token && HttpInterceptorService.isNotRedirect(req.url)) {
      req = req.clone({
        setHeaders: {
          authorization: `Bearer ${token}`
        }
      })
    }

    return next.handle(req).pipe(
      catchError((err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.authService.removeSecurityContext()
            this.router.navigate(['login']);
          }
        }
        return throwError(err);
      })
    )
  }

  private static isNotRedirect(url: string): boolean {
    return !url.includes('token');
  }
}
