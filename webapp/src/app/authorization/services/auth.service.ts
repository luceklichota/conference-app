import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { BehaviorSubject, Observable, tap } from "rxjs";

export type TokenInfo = {
  access_token: string;
  expires_in: number;
  id_token: string;
  refresh_token: string;
  token_type: string;
}

export type User = {
  firstName: string;
  lastName: string;
  email: string;
  city: string;
}

export type ProfileResponse = {
  ['custom:city']: string;
  email: string;
  email_verified: string;
  family_name: string;
  given_name: string
  identities: string;
  sub: string;
  username: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  public tokenReceived$ = new BehaviorSubject(false);
  public userProfile$ = new BehaviorSubject({
    firstName: '',
    lastName: '',
    email: '',
    city: ''
  })

  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) {
  }

  public loadToken(): void {
    this.route.queryParams.subscribe(params => {
      const code = params['code'];
      if (code) {
        const clientId = window.appConfig.awsUserPoolWebClientId
        const redirectUri = window.appConfig.baseUrl + 'token'
        const domain = window.appConfig.domain

        const params = new HttpParams()
          .append('grant_type', 'authorization_code')
          .append('code', code)
          .append('client_id', clientId)
          .append('redirect_uri', redirectUri)

        const options = {
          headers: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          params
        };

        this.http.post<TokenInfo>(`${domain}/oauth2/token`, '', options)
          .subscribe((credentials: TokenInfo) => {
              AuthService.saveSecurityContext(credentials)
              this.tokenReceived$.next(true)
            }
          )
      }
    });
  }

  public removeSecurityContext(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_in');
    localStorage.removeItem('token_type');
  }

  public loadUserProfile(): Observable<ProfileResponse> {
    return this.http.get<ProfileResponse>(`${window.appConfig.domain}/oauth2/userInfo`)
      .pipe(tap((profile: ProfileResponse) => this.storeProfile(profile)))
  }

  public loginUser(): void {
    const cognitoDomain = window.appConfig.domain
    const clientId = window.appConfig.awsUserPoolWebClientId
    const identityProvider = 'Google'
    const scope = 'phone email openid profile aws.cognito.signin.user.admin'
    const redirectUri = window.appConfig.baseUrl + 'token'
    window.location.href = `${cognitoDomain}/oauth2/authorize?redirect_uri=${redirectUri}&response_type=code&client_id=${clientId}&identity_provider=${identityProvider}&scope=${scope}`
  }

  public logoutUser(): void {
    this.removeSecurityContext()
    this.router.navigate(['/login'])
  }

  private storeProfile(profile: ProfileResponse): void {
    this.userProfile$.next({
      firstName: profile.given_name,
      lastName: profile.family_name,
      email: profile.email,
      city: profile["custom:city"]
    })
  }

  public getAccessToken(): string | null {
    return localStorage.getItem('access_token');
  }

  private static saveSecurityContext(credentials: TokenInfo): void {
    localStorage.setItem('access_token', credentials.access_token)
    localStorage.setItem('id_token', credentials.id_token)
    localStorage.setItem('expires_in', credentials.expires_in + '')
    localStorage.setItem('token_type', credentials.token_type)
  }
}
