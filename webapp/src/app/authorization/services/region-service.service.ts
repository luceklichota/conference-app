import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RegionService {
  constructor(private httpClient: HttpClient) {
  }

  public getRegions(): Observable<string[]> {
    return of(["Warszawa","Lublin","Poznań","Białystok","Kielce","Zdalnie"]);
  }

  public saveUserRegion(city: string): Observable<{}> {
    return this.httpClient.post(`${window.appConfig.apiUrl}/user-city/`, {city})
  }
}
