import { Component, OnInit } from '@angular/core';
import { EventService } from "../services/event.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public myVenues: any;
  public isLoading = false;

  constructor(private eventService: EventService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.eventService.getMyVenues()
      .subscribe((data) => {
        this.myVenues = data
        this.isLoading = false;
      })
  }

}
