import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../authorization/services/auth.service";
import { NavigationEnd, Router } from "@angular/router";
import { filter, tap } from "rxjs";

@Component({
  selector: 'app-event-box-container',
  templateUrl: './event-box-container.component.html',
  styleUrls: ['./event-box-container.component.scss']
})
export class EventBoxContainerComponent implements OnInit {
  isEventPreview = false;
  constructor(private authService: AuthService, private router: Router) {
  }
  ngOnInit(): void {
    this.authService.loadUserProfile().subscribe();
    this.isEventPreview = this.router.url.split('/').length === 4
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      tap((evt: any) => {this.isEventPreview = evt.url.split('/').length === 4})
    ).subscribe()
  }

}
