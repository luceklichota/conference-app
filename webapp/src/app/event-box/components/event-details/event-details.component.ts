import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Subscription, tap } from "rxjs";
import { EventService } from "../services/event.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {
  public isLoading = false;
  private id: any;
  private routeSub: Subscription;
  public venue: any;
  public venueEvents: any;
  constructor(private route: ActivatedRoute, private eventService: EventService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.routeSub = this.route.params
      .pipe(
        tap((params) => this.getDetails(params['id'])),
        tap((params) => this.getVenueEvents(params['id'])),
      )
      .subscribe();
  }

  private getDetails(id: string): void {
    this.isLoading = true;
    this.id = id;
    this.eventService.getVenue(id)
      .subscribe((data) => {
        this.venue = data
        this.isLoading = false;
      })
  }

  public getVenueEvents(id: string) {
    this.eventService.getVenueEvents(id)
      .subscribe((data) => {
        this.venueEvents = data;
      })
  }


  public manageAttending(id: string, isAttending: boolean): void {
    if (!isAttending) {
      this.attendVenue(id)
    } else {
      this.unAttend(id);
    }
  }

  public manageEventAttending(id: string, isAttending: boolean): void {
    if (!isAttending) {
      this.attendEvent(id)
    } else {
      this.unAttendEvent(id);
    }
  }


  attendEvent(id: string){
    this.eventService.attendEvent(id)
      .subscribe({
        next: () => {
          this.getVenueEvents(this.id)
          this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Zostałeś zapisany na wydarzenie', life: 3000});
        },
        error: () => {
          this.messageService.add({key: 'tc', severity:'error', summary: 'Wystąpił problem', detail: 'Coś poszło nie tak', life: 3000});
        }
      })
  }

  unAttendEvent(id: string){
    this.eventService.unAttendEvent(id)
      .subscribe({
        next: () => {
          this.getVenueEvents(this.id)
          this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Zostałeś wypisany z wydarzenia', life: 3000});
        },
        error: () => {
          this.messageService.add({key: 'tc', severity:'error', summary: 'Wystąpił problem', detail: 'Coś poszło nie tak', life: 3000});
        }
      })
  }


  private unAttend(id: string): void {
    this.eventService.unAttendVenue(id)
      .subscribe({
        next: () => {
          this.getDetails(this.id)
          this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Zostałeś wypisany z eventu', life: 3000});
        },
        error: () => {
          this.messageService.add({key: 'tc', severity:'error', summary: 'Wystąpił problem', detail: 'Coś poszło nie tak', life: 3000});
        }
      })
  }

  private attendVenue(id: string): void {
    this.eventService.attendVenue(id)
      .subscribe({
        next: () => {
          this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Zostałeś zapisany na event', life: 3000});
          this.getDetails(this.id)
        },
        error: () => {
          this.messageService.add({key: 'tc', severity:'error', summary: 'Wystąpił problem', detail: 'Jesteś na liście eventu', life: 3000});
        }
      })
  }

}
