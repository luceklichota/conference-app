import { Component, OnInit } from '@angular/core';
import { EventService, VenuesResponse } from "../services/event.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-venue-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent implements OnInit {
  public venuesList: VenuesResponse[] = [];
  public isLoading: boolean = false;

  constructor(private eventService: EventService, private messageService: MessageService) { }

  public ngOnInit(): void {
    this.isLoading = true;
    this.eventService.getVenues()
      .subscribe((data: VenuesResponse[]) => {
        this.venuesList = data
        this.isLoading = false;
      })
  }

  public manageAttending(id: string, isAttending: boolean): void {
    if (!isAttending) {
      this.attendVenue(id)
    } else {
      this.unAttend(id);
    }
  }

  private attendVenue(id: string): void {
    this.eventService.attendVenue(id)
      .subscribe({
        next: () => {
          this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Zostałeś zapisany na event', life: 3000});
          this.refreshVenues();
        },
        error: () => {
          this.messageService.add({key: 'tc', severity:'error', summary: 'Wystąpił problem', detail: 'Jesteś na liście eventu', life: 3000});
        }
      })
  }

  private unAttend(id: string): void {
    this.eventService.unAttendVenue(id)
      .subscribe({
        next: () => {
          this.messageService.add({key: 'tc', severity:'success', summary: 'Gratulacje', detail: 'Zostałeś wypisany z eventu', life: 3000});
          this.refreshVenues();
        },
        error: () => {
          this.messageService.add({key: 'tc', severity:'error', summary: 'Wystąpił problem', detail: 'Coś poszło nie tak', life: 3000});
        }
      })
  }

  private refreshVenues(): void {
    this.isLoading = true;
    this.eventService.getVenues()
      .subscribe((data: VenuesResponse[]) => {
        this.venuesList = data;
        this.isLoading = false;
      })
  }
}
