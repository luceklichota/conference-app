import { Pipe, PipeTransform } from '@angular/core';
import { VenuesResponse } from "../services/event.service";
import { format } from 'date-fns';

@Pipe({
  name: 'formatDay'
})
export class FormatDayPipe implements PipeTransform {

  transform(value: string, venue: VenuesResponse): unknown {
    return `${format(new Date(value), 'dd')}-${format(new Date(venue.endDate), 'dd.MM')}`
  }

}
