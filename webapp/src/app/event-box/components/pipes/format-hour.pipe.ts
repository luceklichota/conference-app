import { Pipe, PipeTransform } from '@angular/core';
import { format } from "date-fns";

@Pipe({
  name: 'formatHour'
})
export class FormatHourPipe implements PipeTransform {

  transform(value: string): unknown {
    return `${format(new Date(), 'HH:MM')}`
  }
}
