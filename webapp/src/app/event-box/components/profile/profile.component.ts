import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService, User } from "../../../authorization/services/auth.service";
import { Subject, switchMap, takeUntil, tap } from "rxjs";
import { RegionService } from "../../../authorization/services/region-service.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  public filteredRegions: any[] = [];
  public showRegionSelect = false;
  public controlModel: any;
  private regions: string[] = []
  public user: User = {
    firstName: '',
    lastName: '',
    email: '',
    city: ''
  };
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(public authService: AuthService, private regionService: RegionService) {
  }

  public ngOnInit(): void {
    this.authService.userProfile$
      .pipe(takeUntil(this.destroy$))
      .subscribe((userProfile) => {
        this.user = userProfile;
      })

    this.regionService.getRegions()
      .subscribe((regions) => {
        this.regions = regions
    })
  }

  public logoutUser(): void {
    this.authService.logoutUser();
  }

  public saveCity(event: {name: string}): void {
    this.regionService.saveUserRegion(event.name)
      .pipe(
        switchMap(() => this.authService.loadUserProfile()),
        tap(() => this.showRegionSelect = !this.showRegionSelect)
      )
      .subscribe()
  }

  filterCountry(event: any): any {
    let filtered: any = []
    let query = event.query;
    for (let i = 0; i < this.regions.length; i++) {
      let region = this.regions[i];
     if (region.toLowerCase().indexOf(query.toLowerCase()) == 0) {
       filtered.push({ name: region })
     }
    }
    this.filteredRegions = filtered;
  }

  public ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
