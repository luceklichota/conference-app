import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export type VenuesResponse = {
  limit: number,
  endDate: string,
  startDate: string,
  address: string,
  name: string,
  id: string,
  coordinates: string,
  canAttend: boolean,
  isAttending: boolean
  description: string;
}

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private httpClient: HttpClient) { }

  public getVenues(): Observable<VenuesResponse[]> {
    return this.httpClient.get<VenuesResponse[]>(`${window.appConfig.apiUrl}/venues/`);
  }

  public getEvents(): Observable<VenuesResponse[]> {
    return this.httpClient.get<VenuesResponse[]>(`${window.appConfig.apiUrl}/events/`);
  }

  public getVenue(id: string): Observable<any> {
    return this.httpClient.get(`${window.appConfig.apiUrl}/venues/${id}`);
  }

  public getVenueEvents(id: string): Observable<any> {
    return this.httpClient.get(`${window.appConfig.apiUrl}/venues/${id}/events`);
  }

  public attendVenue(id: string): Observable<{}> {
    return this.httpClient.post(`${window.appConfig.apiUrl}/venues/attend/${id}`, {});
  }

  public attendEvent(id: string): Observable<{}> {
    return this.httpClient.post(`${window.appConfig.apiUrl}/events/attend/${id}`, {});
  }

  public unAttendVenue(id: string): Observable<{}> {
    return this.httpClient.post(`${window.appConfig.apiUrl}/venues/unattend/${id}`, {});
  }

  public unAttendEvent(id: string): Observable<{}> {
    return this.httpClient.post(`${window.appConfig.apiUrl}/events/unattend/${id}`, {});
  }

  public getMyVenues(): Observable<VenuesResponse[]> {
    return this.httpClient.get<VenuesResponse[]>(`${window.appConfig.apiUrl}/venues/my/`);
  }

  public deleteVenue(id: string): Observable<any> {
    return this.httpClient.delete(`${window.appConfig.apiUrl}/venues/${id}`)
  }

  public deleteEvent(id: string): Observable<any> {
    return this.httpClient.delete(`${window.appConfig.apiUrl}/events/${id}`)
  }
}
