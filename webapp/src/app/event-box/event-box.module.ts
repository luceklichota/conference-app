import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventBoxContainerComponent } from './components/event-box-container/event-box-container.component';
import { UiDomainModule } from "../ui-domain/ui-domain.module";
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EventListComponent } from './components/event-list/event-list.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from "../authorization/guards/auth-guard.service";
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { FormatDayPipe } from './components/pipes/format-day.pipe';
import { FormsModule } from "@angular/forms";
import '@angular/common/locales/global/pl';
import { FormatHourPipe } from './components/pipes/format-hour.pipe'

const enum PanelRoutes {
  ROOT = '',
  DASHBOARD = 'dashboard',
  EVENT_LIST = 'event-list',
  EVENT = 'event-list/:id',
  PROFILE = 'my-profile'
}

const mainRoutes: Routes = [
  {
    path: PanelRoutes.ROOT, component: EventBoxContainerComponent, children: [
      { path: PanelRoutes.ROOT, pathMatch: 'full', redirectTo: PanelRoutes.DASHBOARD },
      { path: PanelRoutes.DASHBOARD, component: DashboardComponent, canActivate: [AuthGuard] },
      { path: PanelRoutes.EVENT_LIST, component: EventListComponent, canActivate: [AuthGuard] },
      { path: PanelRoutes.EVENT, component: EventDetailsComponent, canActivate: [AuthGuard] },
      { path: PanelRoutes.PROFILE, component: ProfileComponent, canActivate: [AuthGuard] }
    ]
  }
]

@NgModule({
  declarations: [
    EventBoxContainerComponent,
    DashboardComponent,
    EventListComponent,
    ProfileComponent,
    EventDetailsComponent,
    FormatDayPipe,
    FormatHourPipe
  ],
  imports: [
    CommonModule,
    UiDomainModule,
    RouterModule.forChild(mainRoutes),
    FormsModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "pl-PL" }
  ]
})
export class EventBoxModule {
}
