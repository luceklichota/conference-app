import { Component, OnInit } from '@angular/core';
import { NavigationService } from "../../navigation.service";
import { Router } from "@angular/router";
import { MobileNavigationConfig } from "../../model/navigation";
import { AuthService } from "../../../authorization/services/auth.service";

@Component({
  selector: 'app-desktop-navigation',
  templateUrl: './desktop-navigation.component.html',
  styleUrls: ['./desktop-navigation.component.scss']
})
export class DesktopNavigationComponent implements OnInit {
  public activeItemId: number | null = null;
  constructor(public navigationService: NavigationService, private router: Router, public authService: AuthService) {
  }

  public ngOnInit(): void {
    this.markActiveItemOnComponentBootstrap();
  }

  public setActiveItemAdnNavigate(id: number, route: string): void {
    this.activeItemId = id;
    this.router.navigate([`panel/${route}`])
  }

  markActiveItemOnComponentBootstrap(): void {
    const activeRoute: string = this.router.url.split('/')[2]
    this.activeItemId = this.navigationService
      .getNavigationConfig()
      .filter((element: MobileNavigationConfig ) => element.route === activeRoute)
      .map((element: MobileNavigationConfig) => element.id)
      .shift() as number
  }

}
