import { Component, OnInit } from '@angular/core';
import { NavigationService } from "../../navigation.service";
import { Router } from "@angular/router";
import { MobileNavigationConfig } from "../../model/navigation";

@Component({
  selector: 'app-mobile-navigation',
  templateUrl: './mobile-navigation.component.html',
  styleUrls: ['./mobile-navigation.component.scss']
})
export class MobileNavigationComponent implements OnInit {
  public activeItemId: number | null = null;
  constructor(public navigationService: NavigationService, private router: Router) {
  }

  public ngOnInit(): void {
    this.markActiveItemOnComponentBootstrap();
  }

  public setActiveItemAdnNavigate(id: number, route: string): void {
    this.activeItemId = id;
    this.router.navigate([`panel/${route}`])
  }

  markActiveItemOnComponentBootstrap(): void {
    const activeRoute: string = this.router.url.split('/')[2]
    this.activeItemId = this.navigationService
      .getNavigationConfig()
      .filter((element: MobileNavigationConfig ) => element.route === activeRoute)
      .map((element: MobileNavigationConfig) => element.id)
      .shift() as number
  }
}
