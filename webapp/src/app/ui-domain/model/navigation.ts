export interface MobileNavigationConfig {
  displayName: string;
  id: number;
  icon: string;
  route: string;
}
