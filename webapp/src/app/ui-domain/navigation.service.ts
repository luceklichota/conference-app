import { Injectable } from '@angular/core';
import { MobileNavigationConfig } from "./model/navigation";

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  getNavigationConfig(): MobileNavigationConfig[] {
    return [
      { displayName: 'Dashboard', id: 1, icon: 'bell', route: 'dashboard' },
      { displayName: 'Lista Eventów', id: 2, icon: 'calendar', route: 'event-list' },
      { displayName: 'Profil', id: 3, icon: 'user', route: 'my-profile' },
    ]
  }
}
