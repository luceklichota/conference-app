import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { ToastModule } from 'primeng/toast';
import { RippleModule } from "primeng/ripple";
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { TagModule } from 'primeng/tag';
import { GMapModule } from 'primeng/gmap';
import { AccordionModule } from 'primeng/accordion';
import { AvatarModule } from "primeng/avatar";
import { InputTextareaModule } from "primeng/inputtextarea";
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { AutoCompleteModule } from 'primeng/autocomplete';
import {DropdownModule} from 'primeng/dropdown';

const modules = [
  InputTextModule,
  ButtonModule,
  ToastModule,
  RippleModule,
  ProgressSpinnerModule,
  MessageModule,
  MessagesModule,
  TagModule,
  GMapModule,
  AccordionModule,
  AvatarModule,
  InputTextareaModule,
  CalendarModule,
  ChipsModule,
  AutoCompleteModule,
  DropdownModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...modules
  ],
  exports: [
    ...modules
  ]
})
export class PrimeComponentsModule {
}
