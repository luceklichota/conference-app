import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeComponentsModule } from "./prime-compontents/prime-compontents.module";
import { ContainerComponent } from './components/container/container.component';
import { MobileNavigationComponent } from './components/mobile-navigation/mobile-navigation.component';
import { CardComponent } from './components/card/card.component';
import { DesktopNavigationComponent } from './components/desktop-navigation/desktop-navigation.component';

const modules = [
  PrimeComponentsModule
]

const components = [
  ContainerComponent,
  MobileNavigationComponent,
  CardComponent
]

@NgModule({
  declarations: [
    ...components,
    DesktopNavigationComponent,
  ],
  imports: [
    CommonModule,
    ...modules
  ],
    exports: [
        ...modules,
        ...components,
        DesktopNavigationComponent,
    ]
})
export class UiDomainModule {
}
