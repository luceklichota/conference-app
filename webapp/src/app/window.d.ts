export {};

declare global {
  interface Window {
    appConfig: {
      awsUserPoolId: string;
      awsUserPoolWebClientId: string;
      domain: string;
      baseUrl: string;
      apiUrl: string;
    };
  }
}
